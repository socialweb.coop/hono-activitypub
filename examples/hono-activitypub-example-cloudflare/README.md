# hono-activitypub-example-cloudflare

Example of using hono-activitypub and deploying to cloudflare.

This package was created using [Deploy a Hono site](https://developers.cloudflare.com/pages/framework-guides/deploy-a-hono-site/) in cloudflare pages docs.

## How it Works

1. `npm run prepare` uses 11ty to build html from this directory to `./dist/public/`
2. workerd (and/or cloudflare pages) conventionally makes `./dist/public/` files available in an `ASSETS` worker that the other worker can fetch from. The `/*` handler in src/server.js serves requests from ASSETS (which is populated from whatever is built into `./dist/public/`)
3. The `/*` handler in src/server.js also uses `ActivityPubHono.middleware()`, which intercepts any requests from ActivityPub clients (which prefer JSON), fetches the HTML from the same URL, and converts it to an ActivityPub Object.

### In practice

1. /actor/index.md is `i am actor`
2. `npm run prepare` uses 11ty to build that to /dist/public/actor/index.html `<p>i am actor</p>`
3. when the server handles requests for /actor/ with header `Accept: text/html`, the response is
    ```html
    <p>i am actor</p>
    ```
4. when the server handles requests for /actor/ with header `Accept: application/ld+json; profile="https://www.w3.org/ns/activitystreams"`, the response is
    ```json
    {
      "type": [
        "Page"
      ],
      "content": "<p>i am actor</p>\n<p>foo</p>\n",
      "@context": [
        "https://www.w3.org/ns/activitystreams"
      ]
    }
    ```

## Development

### Install Dependencies

```
npm install
```

### Run Dev Server (in workerd)

```
npm run dev
```

Try
```shell
# html
curl http://localhost:8788/actor/

# activitypub
curl -H 'Accept: application/ld+json; profile="https://www.w3.org/ns/activitystreams"' http://localhost:8788/actor/
```
Note: the `content` html may have had `live-reload` stuff injected in by `wrangler pages dev`

### Deploy to Cloudflare

```
npm run deploy
```

### Run in node.js (POC)

```shell
PORT=8083 node src/server-node.js
```

Try

```shell
# html
curl http://localhost:8083/actor

# activitypub
curl -H 'Accept: application/ld+json; profile="https://www.w3.org/ns/activitystreams"' http://localhost:8083/actor
```
