import { serve } from '@hono/node-server'
import { serveStatic } from '@hono/node-server/serve-static'
import { Hono } from "hono"
import ActivityPubHono from "hono-activitypub"

const app = new Hono().use('*', ActivityPubHono.middleware(), serveStatic({
  // serveStatic doesn't let this be an abs path
  root: 'dist/public/'
}))

serve({
  ...app,
  port: process.env.PORT ? parseInt(process.env.PORT, 10) : 0,
  hostname: process.env.HTTP_HOSTNAME || undefined,
}, (info) => {
  console.warn(`http://localhost:${info.port}`)
})
