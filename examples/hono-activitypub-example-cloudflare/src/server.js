import { Hono } from 'hono'
import ActivityPubHono from "hono-activitypub"

/**
 * @type {Hono<CloudflareHonoEnv, any, "/">}
 */
const app = new Hono()
app.route("/actor", ActivityPubHono.create())
app.use("*", ActivityPubHono.middleware())
app.get("/*", fetchHtml);

/**
 * @template {string} P
 * @template {import('hono').Input} I
 * @param {import('hono').Context<CloudflareHonoEnv, P, I>} ctx
 */
async function fetchHtml(ctx) {
  if ( ! ctx.env?.ASSETS) {
    return ctx.newResponse('no assets in ctx', 404)
  }
  const reqUrl = new URL(ctx.req.url)
  const assetRequest = new Request(new URL(`/public${reqUrl.pathname}`, reqUrl))
  return await ctx.env.ASSETS.fetch(assetRequest);
}

export default app

/**
 * @typedef {object} CloudflareHonoBindings
 * @property {{
*  fetch: (req: Request) => Promise<Response>
* }} ASSETS
*/

/**
* @typedef CloudflareHonoEnv
* @property {CloudflareHonoBindings} [Bindings]
* @property {Record<string,unknown>} [Variables]
*/
