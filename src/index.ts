import { serve } from '@hono/node-server'
import { Hono } from 'hono'
import * as activityPubHono from './activitypub-hono.js'
import { randomUUID } from 'crypto';

const port = parseInt(process.env.PORT || '3000');

serve({
  ...activityPubHono.use(new Hono, { randomUUID }),
  port
}).addListener('listening', () => {
  console.log(`Server is running on port ${port}`)
})
