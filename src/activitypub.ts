/**
 * outboxes, when sent an object that is not an activity, are supposed to wrap the object in a Create Activity.
 */
export class OutboxActivity {
  static copiedAudiencePropertyNames = new Set(["to", "bto", "cc", "bcc", "audience"])
  static activityTypes = new Set(['Activity', 'IntransitiveActivity', 'Accept', 'Add', 'Announce', 'Arrive', 'Block', 'Create', 'Delete', 'Dislike', 'Flag', 'Follow', 'Ignore', 'Invite', 'Join', 'Leave', 'Like', 'Listen', 'Move', 'Offer', 'Question', 'Reject', 'Read', 'Remove', 'TentativeReject', 'TentativeAccept', 'Travel', 'Undo', 'Update', 'View' ])
  /**
   * return whether the provided object is an AS2 Activity or subclass of one.
   * based on a well-defined allowlist of Activity 'type' values.
   * @param {*} object 
   * @param {Set<string>} activityTypes 
   * @returns 
   */
  static isActivity(object, activityTypes=OutboxActivity.activityTypes) {
    const typeValue = object.type ?? []
    const types = Array.isArray(typeValue) ? typeValue : [typeValue]
    for (const t of types) {
      if (activityTypes.has(t)) {
        return true
      }
    }
    return false
  }
  /**
   * @param {{}} object
   * @param {Set<string>} audiencePropertyNames 
   */
  static audienceProperties(object, audiencePropertyNames=OutboxActivity.copiedAudiencePropertyNames) {
    const entries = Object.entries(object).filter(([key, val]) => audiencePropertyNames.has(key))
    return Object.fromEntries(entries)
  }
  /**
   * @param {{}} object
   * @param {object} options
   * @param {() => string} options.randomUUID
   */
  static wrap(object, { randomUUID }) {
    // should provision an id
    const id = new URL(`urn:uuid:${randomUUID()}`).toString()
    // should copy audience properties from object to new wrapped Create
    const audienceProperties = OutboxActivity.audienceProperties(object)
    object = {
      ...object,
      id,
    }
    return new OutboxActivity({
      id,
      object,
      ...audienceProperties,
    })
  }
  /**
   * @param {{
   *  id: string
   *  type?: string | Array<string>
   * object: {}
   *  "@context"?: string | Array<string>
   * }} options 
   */
  constructor(options) {
    const {
      id,
      object,
      type = ['Create'],
    } = options
    const ldContext = options['@context'] ?? ["https://www.w3.org/ns/activitystreams"]
    Object.assign(this, {
      ...options,
      id,
      type,
      object,
      "@context": ldContext,
    })
  }
}


export class AS2 {
  type: Array<string>
  "@context" = [ "https://www.w3.org/ns/activitystreams"]
  constructor(options:{type?:string[]}={type:[]}) {
    this.type = options.type ?? []
  }
}

// https://www.w3.org/TR/activitystreams-vocabulary/#actor-types
type ActivityVocabularyActorTypeString = "Person" | "Application" | "Group" | "Organization" | "Service"
export type ActorTypeIdentifier = ActivityVocabularyActorTypeString

/**
 * @template {[t1: ActorTypeIdentifier, ...tN: ActorTypeIdentifier[]]} type
 */
export class Actor extends AS2 {
  declare type: ActorTypeIdentifier[]
  inbox?: URL[]
  outbox?: URL[]
  following?: URL[]
  followers?: URL[]
  liked?: URL[]
  likes?: URL[]
  shares?: URL[]
  constructor(
    options: Omit<AS2, '@context'> & {
      type: [actorType: ActorTypeIdentifier, ...otherTypes: string[]]
    }
  ){
    super(options)
    for (const prop of /** @type {const} */ ([
      'inbox', 'outbox', 'following', 'followers',
      'liked', 'likes', 'shares',
    ])) {
      this[prop] = options[prop]
    }
  }
}

export class OrderedCollection extends AS2 {
  type = ["OrderedCollection"]
  constructor(
    options: { type?: AS2['type'], items?: AS2[] }={},
  ) {
    const type = options.type?.includes('OrderedCollection')
      ? options.type
      : [
          "OrderedCollection",
          ...(options.type ?? []),
        ]
    super({
      ...options,
      type,
    })
  }
}

class Note extends AS2 {
  type = ["Note"]
  constructor(
    public content: string,
    public mediaType: string = 'text/plain',
  ) { super() }
}
