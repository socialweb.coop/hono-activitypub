import { Hono, HonoRequest } from "hono"
import * as AP from "./activitypub.js"
import * as parse5 from "parse5"

// https://www.w3.org/TR/activitypub/#retrieving-objects
const activitypubClientAcceptHeader = `application/ld+json; profile="https://www.w3.org/ns/activitystreams"`

function isActivityStreamsScriptType(scriptType) {
  switch (scriptType) {
    case "application/ld+json":
      return true
  }
  return false
}

/**
 * 
 * @returns {import('hono').MiddlewareHandler}
 */
export function middleware() {
  return async (ctx, next) => {
    if (ctx.req.header('accept') !== activitypubClientAcceptHeader) {
      // not an activitypub request, do nothing
      return await next()
    }
    const htmlResponse = await fetch(ctx.req.url, {
      headers: {
        accept: 'text/html'
      }
    })
    if (htmlResponse.status !== 200) {
      // cant get html so cant return activitypub object
      return next()
    }
    const htmlText = await htmlResponse.text()
    const contentParsed = parse5.parse(htmlText)
    // find script tags
    const queue: any[] = [contentParsed]
    const scripts = []
    while (queue.length) {
      const node = queue.pop()
      if (node.childNodes) for (const child of node.childNodes) {
        if (child.tagName === 'script') {
          if (child.attrs.find(a => isActivityStreamsScriptType(a.value))) {
            scripts.push(child)
          }
        }
        queue.push(child)
      }
    }
    function * walkText(node) {
      if (node.nodeName === '#text') {
        yield node.value
      } else {
        if (node.childNodes) for (const child of node.childNodes) {
          yield * walkText(child)
        }
      }
    }
    let objectFromHtml
    // use each script
    for (const script of scripts) {
      const json = String(...walkText(script))
      const parsed = JSON.parse(json)
      const parsedContext = ('@context' in parsed) ? parsed['@context'] : []
      if (Array.isArray(parsedContext) && parsedContext.includes("https://www.w3.org/ns/activitystreams")) {
        objectFromHtml = parsed
        break;
      }
    }
    const object = {
      ...objectFromHtml,
      type: ['Page','Service'],
      content: htmlText,
      '@context': Array.from(new Set([
        "https://www.w3.org/ns/activitystreams",
        ...objectFromHtml['@context'] || [],
      ]))
    }
    const body = JSON.stringify(object, undefined, 2)
    return new Response(body, { status: 200 })
  }
}

/**
 * create a Hono for an ActivityPub Actor
 */
export const use = <H extends Hono>(
  app: H,
  options: {
    randomUUID: () => string
    /** get a full URL from the request (take into account x-forwarded-proto if you want) */
    url?: (request: Pick<Request, 'url'|'headers'|'body'>) => Promise<URL>
  }
) => {
  const { randomUUID } = options
  const toUrl = async (request: Request) => {
    if (options.url) {
      return await options.url(request)
    }
    const proto = 'http'
    if (request.url.startsWith('/')) {
      return new URL(`${proto}://${request.headers.get('host')}${request.url}`)
    }
    return new URL(request.url)
  }
  const urls = async (req: HonoRequest) => {
    return ({
      inbox: [new URL('inbox', await toUrl(req.raw))],
      outbox: [new URL('outbox',  await toUrl(req.raw))],
      following: [new URL('following',  await toUrl(req.raw))],
      followers: [new URL('followers',  await toUrl(req.raw))],
      liked: [new URL('liked',  await toUrl(req.raw))],
      likes: [new URL('likes',  await toUrl(req.raw))],
      shares: [new URL('shares',  await toUrl(req.raw))],
    })
  }

  app.get('/activitypub-actor-tester', (c) => {
    return c.render(`
      <script
        src="https://cdn.jsdelivr.net/npm/activitypub-actor-tester@0.3.1/dist/activitypub-actor-tester.js"
        type="module"
        crossorigin="anonymous"
      ></script>
      <activitypub-actor-tester
        actor="."
        test="true"
      ></activitypub-actor-tester>

    `)
  })

  // home page of an actor on the social web
  app.use('/', async (c, next) => {
    if (! c.req.header('accept').includes(activitypubClientAcceptHeader)) {
      return next()
    }
    c.status(200)
    return c.json(new AP.Actor({
      ...await urls(c.req),
      type: ['Service', 'Page'],
    }))
  })

  // actor's inbox - how other activitypub clients delvier messages to the above actor
  app.get('/inbox', (c) => {
    c.status(200)
    return c.json(new AP.OrderedCollection({ items: [] }))
  })
  app.post('/inbox', c => {
    c.status(405)
    return c.json({
      type: 'Error',
      message: 'post handler not yet implemented',
    })
  })

  // actor's outbox - how the actor controller issues commands to the actor
  app.get('/outbox', (c) => {
    c.status(200)
    return c.json(new AP.OrderedCollection({ items: [] }))
  })
  app.post('/outbox', async c => {
    const location = new URL('/outbox/item', await toUrl(c.req.raw))
    const requestBodyBuffer = await c.req.arrayBuffer()
    const requestBodyText = new TextDecoder().decode(requestBodyBuffer)
    const requestBodyObject = JSON.parse(requestBodyText)
    const create = AP.OutboxActivity.isActivity(requestBodyObject) ? new AP.OutboxActivity(requestBodyObject) : AP.OutboxActivity.wrap(requestBodyObject, {randomUUID})
    location.searchParams.set('content', JSON.stringify(create))
    return c.json({
      location: location.toString()
    }, 201, {
      location: location.toString()
    })
  })
  // get handler for the result of POST /outbox
  app.get('outbox/item', async c => {
    const contentText = new URL(c.req.url).searchParams.get('content')
    const content = JSON.parse(contentText)
    return c.json(content)
  })

  app.get('/following', (c) => {
    c.status(200)
    return c.json(new AP.OrderedCollection({ items: [] }))
  })
  app.get('/followers', (c) => {
    c.status(200)
    return c.json(new AP.OrderedCollection({ items: [] }))
  })
  app.get('/liked', (c) => {
    c.status(200)
    return c.json(new AP.OrderedCollection({ items: [] }))
  })
  app.get('/likes', (c) => {
    c.status(200)
    return c.json(new AP.OrderedCollection({ items: [] }))
  })
  app.get('/shares', (c) => {
    c.status(200)
    return c.json(new AP.OrderedCollection({ items: [] }))
  })

  return app  
}

export const create = ({
  app = new Hono,
  randomUUID = () => crypto.randomUUID(),
  url,
}:{
  app?: Hono
  randomUUID?: () => string
  url?: (request: Pick<Request, 'url'|'headers'|'body'>) => Promise<URL>
}={}) => {
  return use(app, { randomUUID, url })
}

export default {
  create,
  middleware,
}
