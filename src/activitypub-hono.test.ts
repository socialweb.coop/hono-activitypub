import * as nodeTest from 'node:test'
import activityPubHonoDefault from './activitypub-hono.js'
import { ActorUrlTestRun } from "activitypub-testing/test-actor";
import { Hono } from 'hono';
import { serve } from '@hono/node-server'
import { Server } from 'node:http';
import assert from 'node:assert'

testActivityPubHono(activityPubHonoDefault, { ...nodeTest, assert })

async function testActivityPubHono(
  activityPubHono: typeof activityPubHonoDefault,
  { describe, test, assert }: typeof nodeTest & { assert: typeof import('node:assert') },
) {
  await describe('activitypub-hono', async () => {

    await test('can use actor.inbox', async () => {
      const app = activityPubHono.create()
      const server = serve({
        fetch: app.fetch,
        port: 0
      })
      await new Promise((resolve) => server.addListener('listening', resolve))
      const actorLink = getAddressUrl(server.address())
      console.log('actorLink', String(actorLink))
      try {
        const response = await fetch(actorLink, {
          headers: {
            accept: `application/ld+json; profile="https://www.w3.org/ns/activitystreams"`
          }
        })
        assert.equal(response.status, 200)
        const actor = await response.json()
        const inboxLink = new URL(actor.inbox, actorLink)
        const inboxResponse = await fetch(inboxLink, {
          headers: {
            accept: `application/ld+json; profile="https://www.w3.org/ns/activitystreams"`
          }
        })
        const inboxText = await inboxResponse.text()
        const inbox = JSON.parse(inboxText)
        assert.equal(typeof inbox, 'object')
        assert.equal(inbox.type.includes('OrderedCollection'), true)
      } finally {
        server.close()
      }
    })

    await test('can run ActorUrlTestRun', async () => {
      const hono = activityPubHono.create()
      const server = serve({
        fetch: hono.fetch,
        port: 0
      })
      await new Promise((resolve) => server.addListener('listening', resolve))
      try {
        const assertions = new ActorUrlTestRun(getAddressUrl(server.address()))
        for await (const a of assertions) {
          switch (a.result.outcome) {
            case "passed":
              continue
            default: {
              console.log(JSON.stringify(a, undefined, 2))
              throw Object.assign(new Error('unexpected outcome'), { assertion: a })
            }
          }
        }
      } finally {
        server.close()
      }
    })
  })
}

function getAddressUrl(address: ReturnType<Server['address']>) {
  switch (typeof address) {
    case "object":
      return new URL(`http://${address.address}:${address?.port}`)
  }
  throw new Error(`unexpected address type: ${typeof address}`)
}
